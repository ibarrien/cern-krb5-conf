require 'spec_helper'

describe package('cern-krb5-conf') do
  it { should_not be_installed }
end

describe package('cern-krb5-conf-defaults-cernch') do
  it { should_not be_installed }
end

describe package('cern-krb5-conf-realm-cernch') do
  it { should_not be_installed }
end

describe file('/etc/krb5.conf.d/cern-defaults-cernch.conf') do
  it { should_not exist }
end

describe file('/etc/krb5.conf.d/cern-realm-cernch.conf') do
  it { should_not exist }
end
