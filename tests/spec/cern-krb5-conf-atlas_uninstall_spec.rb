require 'spec_helper'

describe package('cern-krb5-conf-atlas') do
  it { should_not be_installed }
end

describe package('cern-krb5-conf-defaults-cernch') do
  it { should_not be_installed }
end

describe package('cern-krb5-conf-realm-cernch-atlas') do
  it { should_not be_installed }
end

describe file('/etc/krb5.conf.d/cern-defaults-cernch.conf') do
  it { should_not exist }
end

describe file('/etc/krb5.conf.d/cern-realm-cernch-atlas.conf') do
  it { should_not exist }
end
