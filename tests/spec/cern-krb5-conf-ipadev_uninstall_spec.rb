require 'spec_helper'

describe package('cern-krb5-conf-ipadev') do
  it { should_not be_installed }
end

describe package('cern-krb5-conf-defaults-ipadev') do
  it { should_not be_installed }
end

describe package('cern-krb5-conf-realm-ipadev') do
  it { should_not be_installed }
end

describe file('/etc/krb5.conf.d/cern-defaults-ipadev.conf') do
  it { should_not exist }
end

describe file('/etc/krb5.conf.d/cern-realm-ipadev.conf') do
  it { should_not exist }
end
